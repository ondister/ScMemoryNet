﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Управление общими сведениями о сборке осуществляется с помощью 
// набора атрибутов. Измените значения этих атрибутов, чтобы изменить сведения,
// связанные со сборкой.
[assembly: AssemblyTitle("ScEngineNet")]
[assembly: AssemblyDescription("Враппер для библиотеки sc-memory")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Sapfir Goup")]
[assembly: AssemblyProduct("ScEngineNet")]
[assembly: AssemblyCopyright("Copyright © Sapfir Goup 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Параметр ComVisible со значением FALSE делает типы в сборке невидимыми 
// для COM-компонентов.  Если требуется обратиться к типу в этой сборке через 
// COM, задайте атрибуту ComVisible значение TRUE для этого типа.
[assembly: ComVisible(false)]

// Следующий GUID служит для идентификации библиотеки типов, если этот проект будет видимым для COM
[assembly: Guid("31f29c71-e93c-4402-bff5-ee5f7bd65200")]

// Сведения о версии сборки состоят из следующих четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии 
//   Номер сборки
//      Редакция
//
// Можно задать все значения или принять номера сборки и редакции по умолчанию 
// используя "*", как показано ниже:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.2")]
[assembly: AssemblyFileVersion("1.0.0.2")]
[assembly: InternalsVisibleTo("ScMachineWrapperTest, PublicKey=002400000480000094000000060200000024000052534131000400000100010025547be8da12dbb94012f6fc282a62b80b50c97813cdaf2d580efbc80244dd0491ca57b7f9af8ae6316f6c274c89a0fcbab58d7c6a1429c129c9bae54969782e62f712abcadbc98756f0db8edde339340ba689102cf20f4235decc791750ccc75d47619228a531aaddb855373d9c6af4a8a1e770003726159cccf3e19874a1e6")]
