﻿namespace ScMachineWrapperTest
{
    /// <summary>
    /// Параметры для тестов
    /// </summary>
    public static class TestParams
    {
        
        public static string ConfigFile = @"d:\develop\ScMemoryNet\DEBUG\sc-memory.ini";
        public static string RepoPath = @"d:\develop\ScMemoryNet\DEBUG\semantic_storage";
        public static string ExtensionPath = @"d:\develop\ScMemoryNet\DEBUG\bin\extensions";
        public static string NetExtensionPath = @"d:\develop\ScMemoryNet\DEBUG\bin";
        
    }
}